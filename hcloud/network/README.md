## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_hcloud"></a> [hcloud](#requirement\_hcloud) | 1.25.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_hcloud"></a> [hcloud](#provider\_hcloud) | 1.25.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [hcloud_network.server](https://registry.terraform.io/providers/hetznercloud/hcloud/1.25.1/docs/resources/network) | resource |
| [hcloud_network_subnet.server](https://registry.terraform.io/providers/hetznercloud/hcloud/1.25.1/docs/resources/network_subnet) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ip_range"></a> [ip\_range](#input\_ip\_range) | (Required, string) IP Range of the whole Network which must span all included subnets and route destinations. | `string` | `"10.29.0.0/16"` | no |
| <a name="input_name"></a> [name](#input\_name) | (Required, string) Name of the Network to create (must be unique per project). | `string` | `"server"` | no |
| <a name="input_subnet_ip_range"></a> [subnet\_ip\_range](#input\_subnet\_ip\_range) | (Required, string) Range to allocate IPs from. Must be a subnet of the ip\_range of the Network. | `string` | `"10.29.0.0/24"` | no |
| <a name="input_subnet_network_zone"></a> [subnet\_network\_zone](#input\_subnet\_network\_zone) | (Required, string) Name of network zone. | `string` | `"eu-central"` | no |
| <a name="input_subnet_type"></a> [subnet\_type](#input\_subnet\_type) | (Required, string) Type of subnet (server, cloud or vswitch). | `string` | `"cloud"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_network_id"></a> [network\_id](#output\_network\_id) | The ID of the Network |
| <a name="output_subnet_id"></a> [subnet\_id](#output\_subnet\_id) | The ID of the Subnet |
