resource "hcloud_network" "server" {
  name     = var.name
  ip_range = var.ip_range
}

resource "hcloud_network_subnet" "server" {
  network_id   = hcloud_network.server.id
  type         = var.subnet_type
  network_zone = var.subnet_network_zone
  ip_range     = var.subnet_ip_range
}
