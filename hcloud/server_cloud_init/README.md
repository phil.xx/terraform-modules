## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_hcloud"></a> [hcloud](#requirement\_hcloud) | 1.25.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_hcloud"></a> [hcloud](#provider\_hcloud) | 1.25.1 |
| <a name="provider_template"></a> [template](#provider\_template) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [hcloud_server.node](https://registry.terraform.io/providers/hetznercloud/hcloud/1.25.1/docs/resources/server) | resource |
| [template_file.docker](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_dependancy"></a> [dependancy](#input\_dependancy) | (Required, string) Name of the server type this server should be created with. | `string` | `"module.network.subnet_id"` | no |
| <a name="input_image"></a> [image](#input\_image) | (Required, string) Name or ID of the image the server is created from. | `string` | `"ubuntu-20.04"` | no |
| <a name="input_label"></a> [label](#input\_label) | (Optional, map) User-defined labels (key-value pairs) should be created with. | `string` | `"false"` | no |
| <a name="input_location"></a> [location](#input\_location) | (Optional, string) The location name to create the server in. (nbg1, fsn1 or hel1) | `string` | `"fsn1"` | no |
| <a name="input_name"></a> [name](#input\_name) | (Required, string) Name of the server to create. | `string` | `"server"` | no |
| <a name="input_network_id"></a> [network\_id](#input\_network\_id) | (int) Unique ID of the network. | `string` | `"module.network.network_id"` | no |
| <a name="input_server_type"></a> [server\_type](#input\_server\_type) | (Required, string) Name of the server type this server should be created with. | `string` | `"cx11"` | no |
| <a name="input_ssh_keys"></a> [ssh\_keys](#input\_ssh\_keys) | (Optional, list) SSH key IDs or names which should be injected into the server at creation time. | `string` | `"pub-key"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ipv4_address"></a> [ipv4\_address](#output\_ipv4\_address) | n/a |
| <a name="output_labels"></a> [labels](#output\_labels) | n/a |
