resource "hcloud_server" "node" {
  name        = var.name
  location    = var.location
  image       = var.image
  server_type = var.server_type
  ssh_keys    = var.ssh_keys
  labels = {
    "group" = var.label
  }
  network {
    network_id = var.network_id
  }
  depends_on = [
    var.dependancy
  ]
}
