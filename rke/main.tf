resource rke_cluster "rancher-cluster" {
  cluster_name = var.environment_name
  kubernetes_version = var.kubernetes_version
  ssh_agent_auth = true

  dynamic "nodes" {
    for_each  = var.nodes

    content {
      address = nodes.value["ipv4_address"]

      user    = var.ssh_user
      role    = nodes.value["labels"]["group"] == "controlplane" ? ["controlplane", "etcd"] : ["worker"]
      #ssh_key = file("~/.ssh/id_ed25519.pub")
    }
  }

  network {
    plugin = "calico"
  }
}
