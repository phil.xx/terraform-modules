resource "ionoscloud_lan" "lan" {
  name          = var.name
  datacenter_id = var.datacenter_id
  public        = var.public
}