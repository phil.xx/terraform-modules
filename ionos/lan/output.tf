output "lan_id" {
  description = "The ID of the Network"
  value       = ionoscloud_lan.lan.id
}
