## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_ionoscloud"></a> [ionoscloud](#requirement\_ionoscloud) | 5.0.4 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_ionoscloud"></a> [ionoscloud](#provider\_ionoscloud) | 5.0.4 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [ionoscloud_lan.lan](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/5.0.4/docs/resources/lan) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_name"></a> [name](#input\_name) | (Optional)[string] The name of the LAN. | `string` | `"k8s"` | no |
| <a name="input_public"></a> [public](#input\_public) | (Optional)[Boolean] Indicates if the LAN faces the public Internet (true) or not (false) | `string` | `"true"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_lan_id"></a> [lan\_id](#output\_lan\_id) | The ID of the Network |
