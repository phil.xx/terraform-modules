###################################################################################################
### Default Variables of not set                                                                ###
###################################################################################################
variable "name" {
  description = "(Optional)[string] The name of the LAN."
  default     = "k8s"
}

variable "public" {
  description = "(Optional)[Boolean] Indicates if the LAN faces the public Internet (true) or not (false)"
  default     = "true"
}

variable "datacenter_id" {
  description = "(Required)[string] The ID of a Virtual Data Center."
  default     = "true"
}
