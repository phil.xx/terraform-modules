resource "ionoscloud_server" "node" {
  name              = var.name
  datacenter_id     = var.datacenter_id
  cores             = var.cores
  ram               = var.ram
  //availability_zone = "ZONE_1"
  cpu_family        = var.cpu_family
  //image_password    = "test1234"
  ssh_key_path      = var.ssh_key_path
  //boot_image        = var.boot_image
  image_name        = var.boot_image
  volume {
    name           = var.volume_name
    size           = var.volume_size
    disk_type      = var.volume_disk_type
  }
  nic {
    lan             = var.nic_network_id
    dhcp            = var.nic_dhcp
    //ip              = var.nic_ip
    firewall_active = var.nic_firewall_active
  }
}
