## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_ionoscloud"></a> [ionoscloud](#requirement\_ionoscloud) | 5.0.4 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_ionoscloud"></a> [ionoscloud](#provider\_ionoscloud) | 5.0.4 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [ionoscloud_server.node](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/5.0.4/docs/resources/server) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_boot_image"></a> [boot\_image](#input\_boot\_image) | [string] The image or snapshot UUID / name. | `string` | `"adc59e08-aa35-11eb-8984-e2b74a99fe34"` | no |
| <a name="input_cores"></a> [cores](#input\_cores) | (Required)[integer] Number of server CPU cores. | `string` | `"1"` | no |
| <a name="input_cpu_family"></a> [cpu\_family](#input\_cpu\_family) | (Optional)[string] Sets the CPU type. 'AMD\_OPTERON' or 'INTEL\_XEON'. | `string` | `"AMD_OPTERON"` | no |
| <a name="input_datacenter_id"></a> [datacenter\_id](#input\_datacenter\_id) | (Required)[string] The ID of a Virtual Data Center. | `string` | `"module.datacenter.datacenter_id"` | no |
| <a name="input_name"></a> [name](#input\_name) | (Required)[string] The name of the server. | `string` | `"server"` | no |
| <a name="input_nic_dhcp"></a> [nic\_dhcp](#input\_nic\_dhcp) | (Optional)[Boolean] Indicates if the NIC should get an IP address using DHCP (true) or not (false). | `string` | `"true"` | no |
| <a name="input_nic_firewall_active"></a> [nic\_firewall\_active](#input\_nic\_firewall\_active) | (Optional)[Boolean] If this resource is set to true and is nested under a server resource firewall, with open SSH port, resource must be nested under the NIC. | `string` | `"true"` | no |
| <a name="input_nic_ip"></a> [nic\_ip](#input\_nic\_ip) | (Optional)[string] IP assigned to the NIC. | `string` | `"true"` | no |
| <a name="input_nic_network_id"></a> [nic\_network\_id](#input\_nic\_network\_id) | (Required)[integer] The LAN ID the NIC will sit on. | `string` | `"module.network.network_id"` | no |
| <a name="input_ram"></a> [ram](#input\_ram) | (Required)[integer] The amount of memory for the server in MB. | `string` | `"2048"` | no |
| <a name="input_ssh_key_path"></a> [ssh\_key\_path](#input\_ssh\_key\_path) | (Required)[list] List of paths to files containing a public SSH key that will be injected. Required if image\_password is not provided. | `list` | <pre>[<br>  "~/.ssh/id_ed25519.pub"<br>]</pre> | no |
| <a name="input_volume_disk_type"></a> [volume\_disk\_type](#input\_volume\_disk\_type) | (Required)[string] The volume type: HDD or SSD. | `string` | `"SSD"` | no |
| <a name="input_volume_name"></a> [volume\_name](#input\_volume\_name) | (Optional)[string] The name of the volume. | `string` | `"VOLUME"` | no |
| <a name="input_volume_size"></a> [volume\_size](#input\_volume\_size) | (Required)[integer] The size of the volume in GB. | `string` | `"10"` | no |

## Outputs

No outputs.
