###################################################################################################
### Default Variables of not set                                                                ###
###################################################################################################
variable "name" {
  description = "(Required)[string] The name of the server."
  default     = "server"
}

variable "datacenter_id" {
  description = "(Required)[string] The ID of a Virtual Data Center."
  default     = "module.datacenter.datacenter_id"
}

variable "cores" {
  description = "(Required)[integer] Number of server CPU cores."
  default     = "1"
}

variable "ram" {
  description = "(Required)[integer] The amount of memory for the server in MB."
  default     = "2048" # 1 vCPU, 2 GB RAM, 20 GB NVMe SSD
}

variable "cpu_family" {
  description = "(Optional)[string] Sets the CPU type. 'AMD_OPTERON' or 'INTEL_XEON'."
  default     = "INTEL_SKYLAKE"
}

variable "ssh_key_path" {
  description = "(Required)[list] List of paths to files containing a public SSH key that will be injected. Required if image_password is not provided."
  default     = ["pwelz"]
}

variable "boot_image" {
  description = "[string] The image or snapshot UUID / name."
  default     = "adc59e08-aa35-11eb-8984-e2b74a99fe34" # Ubuntu-20.04-LTS-server-2021-05-01
}

variable "image_name" {
  description = "[string] The image or snapshot UUID / name."
  default     = "adc59e08-aa35-11eb-8984-e2b74a99fe34" # Ubuntu-20.04-LTS-server-2021-05-01
}

variable "volume_name" {
  description = "(Optional)[string] The name of the volume."
  default     = "VOLUME"
}

variable "volume_size" {
  description = "(Required)[integer] The size of the volume in GB."
  default     = "10"
}

variable "volume_disk_type" {
  description = "(Required)[string] The volume type: HDD or SSD."
  default     = "SSD"
}

variable "nic_network_id" {
  description = " (Required)[integer] The LAN ID the NIC will sit on."
  default     = "module.network.network_id"
}

variable "nic_dhcp" {
  description = "(Optional)[Boolean] Indicates if the NIC should get an IP address using DHCP (true) or not (false)."
  default     = "true"
}

variable "nic_ip" {
  description = "(Optional)[string] IP assigned to the NIC."
  default     = "true"
}

variable "nic_firewall_active" {
  description = "(Optional)[Boolean] If this resource is set to true and is nested under a server resource firewall, with open SSH port, resource must be nested under the NIC."
  default     = "true"
}
