###################################################################################################
### Default Variables of not set                                                                ###
###################################################################################################
variable "datacenter_name" {
  description = "(Required)[string] The name of the Virtual Data Center."
  default     = "test"
}

variable "datacenter_location" {
  description = "(Required)[string] The regional location where the Virtual Data Center will be created."
  default     = "de/fra"
}

variable "datacenter_description" {
  description = "(Optional)[string] Description for the Virtual Data Center."
  default     = "Virtual Data Center"
}
