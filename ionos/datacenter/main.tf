resource "ionoscloud_datacenter" "vdc" {
  name        = var.datacenter_name
  location    = var.datacenter_location
  description = var.datacenter_description
}
