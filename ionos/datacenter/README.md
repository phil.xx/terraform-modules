## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_ionoscloud"></a> [ionoscloud](#requirement\_ionoscloud) | 5.0.4 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_ionoscloud"></a> [ionoscloud](#provider\_ionoscloud) | 5.0.4 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [ionoscloud_datacenter.vdc](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/5.0.4/docs/resources/datacenter) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_description"></a> [description](#input\_description) | (Optional)[string] Description for the Virtual Data Center. | `string` | `"Virtual Data Center"` | no |
| <a name="input_location"></a> [location](#input\_location) | (Required)[string] The regional location where the Virtual Data Center will be created. | `string` | `"de/fra"` | no |
| <a name="input_name"></a> [name](#input\_name) | (Required)[string] The name of the Virtual Data Center. | `string` | `"test"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_datacenter_id"></a> [datacenter\_id](#output\_datacenter\_id) | The ID of the Network |
