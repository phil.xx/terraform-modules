output "datacenter_id" {
  description = "The ID of the Network"
  value       = ionoscloud_datacenter.vdc.id
}
