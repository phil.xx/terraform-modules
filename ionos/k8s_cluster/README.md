## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_ionoscloud"></a> [ionoscloud](#requirement\_ionoscloud) | 5.0.4 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_ionoscloud"></a> [ionoscloud](#provider\_ionoscloud) | 5.0.4 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [ionoscloud_k8s_cluster.k8s_cluster](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/5.0.4/docs/resources/k8s_cluster) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_k8s_version"></a> [k8s\_version](#input\_k8s\_version) | (Optional)[string] The desired Kubernetes Version. For supported values, please check the API documentation | `string` | `"1.18.5"` | no |
| <a name="input_name"></a> [name](#input\_name) | (Required)[string] The name of the Kubernetes Cluster. | `string` | `"k8s"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster_id"></a> [cluster\_id](#output\_cluster\_id) | The ID of the Network |
