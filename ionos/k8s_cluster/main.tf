resource "ionoscloud_k8s_cluster" "k8s_cluster" {
  name        = var.name
  k8s_version = var.k8s_version
}