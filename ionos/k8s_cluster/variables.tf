###################################################################################################
### Default Variables of not set                                                                ###
###################################################################################################
variable "name" {
  description = "(Required)[string] The name of the Kubernetes Cluster."
  default     = "k8s"
}

variable "k8s_version" {
  description = "(Optional)[string] The desired Kubernetes Version. For supported values, please check the API documentation"
  default     = "1.18.5"
}
