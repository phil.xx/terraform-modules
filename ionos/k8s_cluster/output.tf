output "cluster_id" {
  description = "The ID of the Network"
  value       = ionoscloud_k8s_cluster.k8s_cluster.id
}
