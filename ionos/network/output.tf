output "network_id" {
  description = "The ID of the Network"
  value       = ionoscloud_lan.network.id
}
