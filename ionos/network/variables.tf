###################################################################################################
### Default Variables of not set                                                                ###
###################################################################################################
variable "datacenter_id" {
  description = "(Required)[string] The ID of a Virtual Data Center."
  default     = "module.datacenter.datacenter_id"
  //default = "d58f22f7-726b-4413-b682-725713222549"
}

variable "public" {
  description = "(Optional)[Boolean] Indicates if the LAN faces the public Internet (true) or not (false)."
  default     = "true"
}

