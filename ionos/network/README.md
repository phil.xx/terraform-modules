## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_ionoscloud"></a> [ionoscloud](#requirement\_ionoscloud) | 5.0.4 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_ionoscloud"></a> [ionoscloud](#provider\_ionoscloud) | 5.0.4 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [ionoscloud_lan.network](https://registry.terraform.io/providers/ionos-cloud/ionoscloud/5.0.4/docs/resources/lan) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_datacenter_id"></a> [datacenter\_id](#input\_datacenter\_id) | (Required)[string] The ID of a Virtual Data Center. | `string` | `"module.datacenter.datacenter_id"` | no |
| <a name="input_public"></a> [public](#input\_public) | (Optional)[Boolean] Indicates if the LAN faces the public Internet (true) or not (false). | `string` | `"true"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_network_id"></a> [network\_id](#output\_network\_id) | The ID of the Network |
