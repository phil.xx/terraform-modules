resource "ionoscloud_lan" "network" {
  datacenter_id = var.datacenter_id
  public        = var.public
}
